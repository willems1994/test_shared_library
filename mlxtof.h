#ifndef MLXTOF_H
#define MLXTOF_H

#ifdef __cplusplus
extern "C" {
#endif


#ifdef PLAT_MINGW
#   define MLXTOF_CALLCONV
#ifdef COMPILING_DLL
#   define DLLEXPORT __declspec(dllexport)
#else
#   define DLLEXPORT __declspec(dllimport)
#endif
#elif defined PLAT_LINUX
    //must be empty
#   define DLLEXPORT
    //must be empty
#   define MLXTOF_CALLCONV
#else
#   error "Please define PLAT_WINDOWS or PLAT_LINUX in your makefile/project"
#endif

int DLLEXPORT create_pipe(void);

#ifdef __cplusplus
}
#endif

#endif
