all:
	make build

cmake_win64:
	mkdir -p build
	cd build && cmake -G"MinGW Makefiles" -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON . in ..

build_win64: cmake
	cd build && mingw32-make

cmake:
	mkdir -p build
	cd build && cmake -G"Unix Makefiles" -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON . in ..

build: cmake
	cd build && make

